Rails.application.routes.draw do
  root to: 'application#index'

  resources :vehicles, only: [:create, :show]
  resources :wash_transactions, only: [:create]

end
