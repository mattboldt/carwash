# == Schema Information
#
# Table name: vehicles
#
#  id             :integer          not null, primary key
#  vehicle_type   :integer
#  license_number :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_vehicles_on_license_number  (license_number)
#

class Vehicle < ApplicationRecord
  has_many :wash_transactions
  validates :vehicle_type, presence: true
  validates :license_number, presence: true
  validate do
    error_msg = 'This car appears to be stolen! The police have been notified'
    self.errors.add(:base, error_msg) if license_number == '1111111'
  end

  enum vehicle_type: [:car, :truck]

  def discount_elligible?
    WashTransaction.where(vehicle: self).count == 1
  end

  def self.find_or_create(vehicle_params)
    vehicle = Vehicle.find_by(license_number: vehicle_params[:license_number])
    vehicle.update(vehicle_params) if vehicle
    vehicle ||= Vehicle.create(vehicle_params)
    vehicle
  end

end
