# == Schema Information
#
# Table name: wash_transactions
#
#  id                :integer          not null, primary key
#  vehicle_id        :integer
#  vehicle_condition :integer          default("normal")
#  bed_ajar          :boolean          default(FALSE)
#  status            :integer          default("pending")
#  price             :float
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_wash_transactions_on_vehicle_id  (vehicle_id)
#

class WashTransaction < ApplicationRecord
  belongs_to :vehicle
  enum vehicle_condition: [:normal, :muddy]
  enum status: [:pending, :washing, :complete]

  validate do
    self.errors.add(:base, 'Your truck bed must be closed') unless bed_ajar == false
  end

  before_create :set_price
  before_create :bump_next_wash

  def set_price
    price = case vehicle.vehicle_type
    when 'car' then 5
    when 'truck' then 10
    end

    price += 2 if muddy?
    price /= 2.0 if vehicle.discount_elligible?

    self.price = price
  end

  # Simulates cars being processed
  def bump_next_wash
    washing = WashTransaction.where(status: :washing).first
    washing.update(status: :complete) if washing

    pending = WashTransaction.where(status: :pending).first
    pending.update(status: :washing) if pending
  end

end
