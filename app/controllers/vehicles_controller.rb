class VehiclesController < ApplicationController

  def show
    @vehicle = Vehicle.find(params[:id])
    @wash_transaction = WashTransaction.new
  end

  def create
    @vehicle = Vehicle.find_or_create(vehicle_params)
    if @vehicle.save
      redirect_to @vehicle
    else
      flash[:alert] = @vehicle.errors.full_messages
      redirect_to root_path
    end
  end

  private

  def vehicle_params
    params.require(:vehicle).permit(:vehicle_type, :license_number)
  end

end
