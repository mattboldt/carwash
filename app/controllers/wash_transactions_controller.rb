class WashTransactionsController < ApplicationController

  before_action :set_vehicle

  def create
    @wash_transaction = WashTransaction.create(wash_transaction_params)
    if @wash_transaction.save

      flash[:success] = "Please give this code to your car care professional: #{SecureRandom.hex(3)} <br>Thank you and have an A1 day"
      redirect_to root_path
    else
      flash[:alert] = @wash_transaction.errors.full_messages
      redirect_to @vehicle
    end
  end

  def show
    render json: WashTransaction.find(params[:id])
  end

  private

  def wash_transaction_params
    params.require(:wash_transaction).permit(:vehicle_id, :vehicle_condition, :bed_ajar)
  end

  def set_vehicle
    @vehicle = Vehicle.find(wash_transaction_params[:vehicle_id])
  end

end
