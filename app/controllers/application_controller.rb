class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def index
    @vehicle = Vehicle.new
    @vehicles = Vehicle.all
    @wash_transactions = WashTransaction.order(created_at: :desc)
                                        .paginate(page: params[:page], per_page: 5)
  end

end
