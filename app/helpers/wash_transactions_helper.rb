module WashTransactionsHelper

  def bootstrap_label_for(status)
    case status
    when 'pending'
      'warning'
    when 'washing'
      'info'
    when 'complete'
      'success'
    end
  end

end
