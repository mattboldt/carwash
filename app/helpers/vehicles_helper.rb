module VehiclesHelper

  def icon_for(type)
    icon = case type
    when 'truck' then 'fa-truck'
    when 'car' then 'fa-car'
    end
    content_tag(:i, '', class: "fa #{icon}")
  end

end
