# Welcome to the A1A Carwash

### Visit the live demo here: https://a1-carwash.herokuapp.com/

_____

#### Development
```
bundle
```

#### Testing
```
bundle exec rspec
```