require 'rails_helper'

RSpec.describe WashTransactionsController, type: :controller do

  describe "POST #create" do
    it "should create and return a single wash" do
      vehicle = Vehicle.create!(license_number: '123456', vehicle_type: :car)
      wash_params = { wash_transaction:
        {vehicle_id: vehicle.id, wash_type: :base, vehicle_condition: :normal, vehicle_restrictions: :permitted}
      }
      expect{
        post :create, params: wash_params
      }.to change(WashTransaction, :count).by(1)
    end

    it "should charge $5" do
      vehicle = Vehicle.create!(license_number: '123456', vehicle_type: :car)
      wash = WashTransaction.create!({vehicle_id: vehicle.id})
      expect(wash.price).to eq(5)
    end

    it "should charge $10" do
      vehicle = Vehicle.create!(license_number: '123456', vehicle_type: :truck)
      wash = WashTransaction.create!({vehicle_id: vehicle.id})
      expect(wash.price).to eq(10)
    end

    it "should charge $12" do
      vehicle = Vehicle.create!(license_number: '123456', vehicle_type: :truck)
      wash = WashTransaction.create!({vehicle_id: vehicle.id, vehicle_condition: :muddy})
      expect(wash.price).to eq(12)
    end

    it "should not create a wash with an ajar truck bed" do
      vehicle = Vehicle.create!(license_number: '123456', vehicle_type: :truck)
      wash = WashTransaction.new({vehicle_id: vehicle.id, vehicle_condition: :muddy, bed_ajar: true})
      expect{wash.save!}.to raise_error(ActiveRecord::RecordInvalid)
    end

    it "should give me a 50 percent off discount" do
      vehicle = Vehicle.create!(license_number: '123456', vehicle_type: :truck)
      WashTransaction.create!({vehicle_id: vehicle.id, vehicle_condition: :muddy, bed_ajar: false})
      wash = WashTransaction.create!({vehicle_id: vehicle.id, vehicle_condition: :normal, bed_ajar: false})
      expect(wash.price).to eq(5)
    end

    it "should not give me a 50 percent off discount" do
      vehicle = Vehicle.create!(license_number: '123456', vehicle_type: :truck)
      WashTransaction.create!({vehicle_id: vehicle.id, vehicle_condition: :muddy, bed_ajar: false})
      WashTransaction.create!({vehicle_id: vehicle.id, vehicle_condition: :normal, bed_ajar: false})
      wash = WashTransaction.create!({vehicle_id: vehicle.id, vehicle_condition: :normal, bed_ajar: false})
      expect(wash.price).to eq(10)
    end
  end

end
