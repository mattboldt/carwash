require 'rails_helper'

RSpec.describe VehiclesController, type: :controller do


  describe "GET #show" do
    it "should return a single vehicle" do
      vehicle = Vehicle.create!(license_number: '123456', vehicle_type: :car)
      get :show, params: {id: vehicle.id}
      expect(subject).to render_template("vehicles/show")
    end
  end

  describe "POST #create" do
    it "should create a vehicle" do
      expect{
        post :create, params: {vehicle: {license_number: '123456', vehicle_type: :car}}
      }.to change(Vehicle, :count).by(1)
    end

    it "should return an existing vehicle" do
      vehicle = Vehicle.create!(license_number: '123456', vehicle_type: :car)
      expect{
        post :create, params: {vehicle: {license_number: '123456', vehicle_type: :car}}
      }.to change(Vehicle, :count).by(0)
    end

    it "should not create a vehicle" do
      vehicle = Vehicle.new(license_number: '1111111', vehicle_type: :car)
      expect{vehicle.save!}.to raise_error(ActiveRecord::RecordInvalid, 'Validation failed: This car appears to be stolen! The police have been notified')
    end
  end

end
