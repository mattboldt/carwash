# == Schema Information
#
# Table name: wash_transactions
#
#  id                :integer          not null, primary key
#  vehicle_id        :integer
#  vehicle_condition :integer          default("normal")
#  bed_ajar          :boolean          default(FALSE)
#  status            :integer          default("pending")
#  price             :float
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_wash_transactions_on_vehicle_id  (vehicle_id)
#

require 'rails_helper'

RSpec.describe WashTransaction, type: :model do

end
