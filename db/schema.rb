# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170313232854) do

  create_table "vehicles", force: :cascade do |t|
    t.integer  "vehicle_type"
    t.string   "license_number"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["license_number"], name: "index_vehicles_on_license_number"
  end

  create_table "wash_transactions", force: :cascade do |t|
    t.integer  "vehicle_id"
    t.integer  "vehicle_condition", default: 0
    t.boolean  "bed_ajar",          default: false
    t.integer  "status",            default: 0
    t.float    "price"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.index ["vehicle_id"], name: "index_wash_transactions_on_vehicle_id"
  end

end
