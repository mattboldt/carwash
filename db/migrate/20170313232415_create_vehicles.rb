class CreateVehicles < ActiveRecord::Migration[5.0]
  def change
    create_table :vehicles do |t|
      t.integer :vehicle_type
      t.string :license_number, index: true

      t.timestamps
    end
  end
end
