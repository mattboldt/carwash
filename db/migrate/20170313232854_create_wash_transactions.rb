class CreateWashTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :wash_transactions do |t|
      t.references :vehicle, index: true
      t.integer :vehicle_condition, default: 0
      t.boolean :bed_ajar, default: false
      t.integer :status, default: 0
      t.float :price
      t.timestamps
    end
  end
end
